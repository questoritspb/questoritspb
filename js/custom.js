'use strict';

//created by NovaVovikov https://github.com/novavovikov


$(document).ready(function () {

	(function ($) {

		var settings = {

			item: '.lang-item',

			activeClass: 'active'

		};

		$.fn.lang = function () {

			if (this.length > 1) {

				this.each($(this).lang());

				return this;
			}

			var el = this;

			var node = {};

			var data = {};

			var methods = {

				init: function init() {

					node.wrap = el;

					node.items = node.wrap.children(settings.item);

					node.items.each(function () {

						var $this = $(this);

						if ($this.data('lang') === 'active') {

							$this.addClass(settings.activeClass);

							$this.detach().prependTo(node.wrap);
						}
					});

					node.items.bind('click', methods.handleLang);

					$(document).bind('click', methods.outsideWrap);
				},

				handleLang: function handleLang() {

					var $this = $(this);

					if ($this.index() !== 0) {

						$this.attr('data-lang', 'active').removeAttr('data-lang');

						$this.addClass(settings.activeClass).siblings().removeClass(settings.activeClass);

						$this.detach().prependTo(node.wrap);

						node.wrap.removeClass(settings.activeClass);
						window.location = $this.attr('data-url');
					} else {

						node.wrap.toggleClass(settings.activeClass);
					}
				},

				outsideWrap: function outsideWrap(e) {

					if (!$(e.target).closest(node.wrap).length) {

						$(node.wrap).removeClass(settings.activeClass);
					}

					e.stopPropagation();
				}

			};

			methods.init();
		};
	})(jQuery);

	$('.lang').lang();

	(function ($) {

		var settings = {

			items: '.menu__item',

			link: 'a',

			activeClass: 'active',

			speed: 600,

			logoScroll: true,

			logoClass: '.logo'

		};

		$.fn.menu = function () {

			if (this.length > 1) {

				this.each($(this).lang());

				return this;
			}

			var el = this;

			var node = {};

			var data = {};

			var methods = {

				init: function init() {

					node.wrap = el;

					node.window = $(window);

					node.html = $('body, html');

					node.header = $('.header');

					node.items = node.wrap.find(settings.items);

					node.links = node.items.find(settings.link);

					data.headerHeight = node.header.outerHeight();

					data.positions = [];

					node.items.bind('click', methods.handleMenu);

					settings.logoScroll ? methods.logoScrollInit() : '';
				},

				handleMenu: function handleMenu() {

					var $this = $(this);

					data.activeSrc = $this.find(settings.link).attr('href');

					node.activeSection = $(data.activeSrc);

					$this.addClass(settings.activeClass).siblings().removeClass(settings.activeClass);

					data.offsetTop = node.activeSection.offset().top - data.headerHeight;

					node.html.stop().animate({

						scrollTop: data.offsetTop

					}, settings.speed, function () {

						data.headerHeight = node.header.outerHeight();

						data.offsetTop = node.activeSection.offset().top - data.headerHeight;

						node.html.stop().animate({

							scrollTop: data.offsetTop

						}, settings.speed / 6);
					});

					return false;
				},

				logoScrollInit: function logoScrollInit() {

					node.logo = $(settings.logoClass);

					node.logo.bind('click', methods.scrollTop);
				},

				scrollTop: function scrollTop() {

					node.html.stop().animate({

						scrollTop: 0

					}, settings.speed);
				}

			};

			methods.init();
		};
	})(jQuery);

	$('.menu').menu();

	(function ($) {

		var settings = {

			className: '_small'

		};

		$.fn.header = function () {

			if (this.length > 1) {

				this.each($(this).lang());

				return this;
			}

			var el = this;

			var node = {};

			var data = {};

			var methods = {

				init: function init() {

					node.header = el;

					node.window = $(window);

					data.headerHeight = el.outerHeight();

					node.window.scroll(methods.scrollWindow);

					node.window.resize(methods.responsive);
				},

				scrollWindow: function scrollWindow() {

					if (node.window.scrollTop() >= data.headerHeight) {

						node.header.addClass(settings.className);
					} else {

						node.header.removeClass(settings.className);
					}
				},

				responsive: function responsive() {

					data.headerHeight = el.outerHeight();
				}

			};

			methods.init();
		};
	})(jQuery);

	$('.header').header();

	//form submit


	/*$('form').on('submit', function () {

		$('.thank__btn').trigger('click');

		return false;
	});*/
	/*feedback from submit*/
    $(document).on('click', ".qv_send_form", function(e) {
		
        var $error = false;
		var $parent = $(this).closest(".qv_form_feedback");
        var $name = $parent.find("input[name='name']");
        var $phone = $parent.find("input[name='phone']");
        var $email = $parent.find("input[name='email']");
        /*if ($name.val() == '') {
            $error = true;
            $name.addClass('input_error');
        } else {
            $name.removeClass('input_error');
        }*/
        if ($phone.val() == '') {
            $error = true;
            $phone.addClass('input_error');
        } else {
            $phone.removeClass('input_error');
        }
		
        if (!$error) {
            $.post('/ajax/send.php', $parent.serialize(), function (data) {
					$('.thank__btn').trigger('click');
					$name.val('');
					$phone.val('');
					$email.val('');
				})
                .fail(function(e) {
                    alert(e.responseText);
                });
        }
        e.preventDefault();
    });
	

	$('.thank__close').click(function () {

		$('.nv-popup__overlay').trigger('click');

		return false;
	});

	//toggle

	$('.menu__toggle').toggleItem();

	//slick

	$('.slider').slick({

		arrows: false,

		speed: 500,

		autoplay: true,

		dots: true,

		autoplaySpeed: 4000

	});

	//mask phone

	$('.__js-phone__').mask('+0 (000) 000-0000');

	//mask phone

	$('.__js-popup__').popup();

	//vieport checker

	$('.tariff-offer').viewportChecker({

		classToAdd: 'slideUp',

		offset: ['5%']

	});

	$('.advantage-item').viewportChecker({

		classToAdd: 'slideUp',

		offset: ['5%']

	});

	$('.slider-item').viewportChecker({

		classToAdd: 'visible',

		offset: ['5%']

	});

	$('.callback__title').viewportChecker({

		classToAdd: 'slideLeft',

		offset: ['5%']

	});

	$('.callback-form').viewportChecker({

		classToAdd: 'slideRight',

		offset: ['5%']

	});

	$('.tariff-option').viewportChecker({

		classToAdd: 'slideRight',

		offset: ['5%']

	});

	$('.scheme li').viewportChecker({

		classToAdd: 'slideRight',

		offset: ['5%']

	});

	$('.terms__arrow').viewportChecker({

		classToAdd: 'slideDown',

		offset: ['5%']

	});
});