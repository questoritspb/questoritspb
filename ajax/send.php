<?php

try {
    if (!empty($_POST)) {
        $arr = [];
        $arr['NAME'] = trim($_POST['name']);
        $arr['PHONE'] = trim($_POST['phone']);
        $arr['EMAIL'] = trim($_POST['email']);
        if (empty($arr["PHONE"])) {
            throw new ErrorException("Ошибка передачи данных");
        } else {
			$to      = 'office@etgroup-mobile.com';
			//$to      = 'kmv@questor.ru';
			$subject = 'Заявка с сайта';
			$message = "
			Новая завяка с сайта!
			Имя: {$arr['NAME']}
			Телефон: {$arr['PHONE']}
			Email: {$arr['EMAIL']}
			";
			$headers = 'From: office@etgroup-mobile.com' . "\r\n" .
				'Reply-To: office@etgroup-mobile.com' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
        }
    } else {
        throw new \Exception("is not ajax");
    }
} catch (Exception $e) {
    //CHTTP::SetStatus("403 Forbidden");
	header("HTTP/1.0 403 Forbidden");
    echo $e->getMessage();
}

?>